package ua.ithillel.car.service.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.service.CarService;
import ua.ithillel.car.service.service.OrderService;

import java.util.List;

@Controller
@RequestMapping("/manager")
public class ManagersController {

    private final CarService carService;
    private final OrderService orderService;

    public ManagersController(CarService carService, OrderService orderService) {
        this.carService = carService;
        this.orderService = orderService;
    }

    @RequestMapping(value = "/orders/delete/{id}", method = RequestMethod.POST)
    @Transactional
    public ModelAndView deleteOrder(@PathVariable("id") int id) {
        carService.updateCarTrue(orderService.findById(id).getCar().getId());
        orderService.deleteOrder(id);
        return new ModelAndView(new RedirectView("/manager/orders"));
    }

    @GetMapping("/orders")
    public String orders(Model model) {
        List<Order> orders = orderService.getAllOrders();
        model.addAttribute("orders", orders);
        return "orders";
    }

    @RequestMapping(value = "/add")
    public String addCar(Model model) {
        model.addAttribute("car", new Car());
        return "addCar";
    }

    @PostMapping("/create")
    public ModelAndView createCar(@ModelAttribute("car") Car car) {
        car.setAvailable(true);
        carService.createCar(car);
        return new ModelAndView(new RedirectView("/manager"));
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public ModelAndView deleteCar(@PathVariable("id") long id) {
        carService.deleteCar(id);
        return new ModelAndView(new RedirectView("/manager"));
    }

}
