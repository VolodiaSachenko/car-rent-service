package ua.ithillel.car.service.controller;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ua.ithillel.car.service.entity.Client;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.service.CarService;
import ua.ithillel.car.service.service.ManagerService;
import ua.ithillel.car.service.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/client")
public class ClientsController {

    private final CarService carService;
    private final OrderService orderService;
    private final ManagerService managerService;

    public ClientsController(CarService carService, OrderService orderService,
                             ManagerService managerService) {
        this.carService = carService;
        this.orderService = orderService;
        this.managerService = managerService;
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.POST)
    @Transactional
    public ModelAndView addOrder(@PathVariable long id, HttpServletRequest request) {
        HttpSession session = request.getSession();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        Order order = new Order();
        order.setDate(new Date(dateFormat.format(calendar.getTime())));
        order.setCar(carService.selectCarById(id));
        order.setManager(managerService.selectRandomManager());
        order.setClient((Client) session.getAttribute("client"));
        orderService.createOrder(order);

        carService.updateCarFalse(id);

        return new ModelAndView(new RedirectView("/client"));
    }

}
