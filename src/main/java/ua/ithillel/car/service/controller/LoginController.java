package ua.ithillel.car.service.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.entity.Client;
import ua.ithillel.car.service.Login;
import ua.ithillel.car.service.entity.Manager;
import ua.ithillel.car.service.service.CarService;
import ua.ithillel.car.service.service.ClientService;
import ua.ithillel.car.service.service.ManagerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class LoginController {

    private final CarService carService;
    private final ClientService clientService;
    private final ManagerService managerService;
    private Client client;
    private Manager manager;

    public LoginController(CarService carService, ClientService clientService, ManagerService managerService) {
        this.carService = carService;
        this.clientService = clientService;
        this.managerService = managerService;
    }

    @GetMapping("/")
    public String index() {
        return "login";
    }

    @PostMapping("/home")
    public String checkLogin(@ModelAttribute Login loginForm, Model model, HttpServletRequest request) {
        return loginForm.isCheckBox() ? loginManager(loginForm, model) : loginClient(loginForm, model, request);
    }

    @GetMapping("/client")
    public String loginClient(Login loginForm, Model model, HttpServletRequest request) {
        if (client == null) {
            client = clientService.authorizationClient(loginForm);
        }
        if (client != null) {
            HttpSession session = request.getSession();
            session.setAttribute("client", client);
            List<Car> cars = carService.getAllCars();
            model.addAttribute("cars", cars);
            return "client";
        } else {
            return "error";
        }
    }

    @GetMapping("/manager")
    public String loginManager(Login loginForm, Model model) {
        if (manager == null) {
            manager = managerService.authorizationManager(loginForm);
        }
        if (manager != null) {
            List<Car> cars = carService.getAllCars();
            model.addAttribute("cars", cars);
            return "manager";
        } else {
            return "error";
        }
    }

}
