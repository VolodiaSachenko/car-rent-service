package ua.ithillel.car.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {
    Order findById(int id);

}
