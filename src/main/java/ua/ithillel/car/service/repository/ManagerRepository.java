package ua.ithillel.car.service.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Manager;

import java.util.Optional;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Integer> {

    Optional<Manager> findByLoginAndPassword(String login, String password);

    Manager findManagerById(int id);

}
