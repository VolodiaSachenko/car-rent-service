package ua.ithillel.car.service.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ithillel.car.service.entity.Car;

import java.util.List;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

    void deleteCarById(long id);

    List<Car> findByOrderByAvailableDesc();

    Car findCarById(long id);

    @Modifying
    @Query("update Car car set car.available = false where car.id = :id")
    void updateFalse(@Param("id") long id);

    @Modifying
    @Query("update Car car set car.available = true where car.id = :id")
    void updateTrue(@Param("id") long id);


}
