package ua.ithillel.car.service.entity;

public enum Role {
    NULL,
    ADMIN,
    MANAGER,
    CLIENT
}
