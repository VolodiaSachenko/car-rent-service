package ua.ithillel.car.service.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "clients")
public class Client {
    @Id
    private int id;
    private String name;
    private String surname;
    private String phone;
    private String login;
    private String password;

    @Override
    public String toString() {
        return "name: " + name + "," +
                " surname: " + surname + "," +
                " phone: " + phone;
    }
}
