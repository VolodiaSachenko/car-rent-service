package ua.ithillel.car.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Login {

    private String login;
    private String password;
    private boolean checkBox;

}

