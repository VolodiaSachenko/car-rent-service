package ua.ithillel.car.service.service;

import org.springframework.stereotype.Service;
import ua.ithillel.car.service.entity.Client;
import ua.ithillel.car.service.Login;
import ua.ithillel.car.service.repository.ClientRepository;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client authorizationClient(Login loginForm) {
        return clientRepository.findByLoginAndPassword(loginForm.getLogin(), loginForm.getPassword()).orElse(null);
    }

}
