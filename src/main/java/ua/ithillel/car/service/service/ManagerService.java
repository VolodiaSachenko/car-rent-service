package ua.ithillel.car.service.service;

import org.springframework.stereotype.Service;
import ua.ithillel.car.service.Login;
import ua.ithillel.car.service.entity.Manager;
import ua.ithillel.car.service.repository.ManagerRepository;

@Service
public class ManagerService {
    private final ManagerRepository managerRepository;

    public ManagerService(ManagerRepository managerRepository) {
        this.managerRepository = managerRepository;
    }

    public Manager authorizationManager(Login login) {
        return managerRepository.findByLoginAndPassword(login.getLogin(), login.getPassword()).orElse(null);
    }

    public Manager selectRandomManager() {
        int randomId = (int) (1 + Math.random() * 9);
        return managerRepository.findManagerById(randomId);
    }

}
