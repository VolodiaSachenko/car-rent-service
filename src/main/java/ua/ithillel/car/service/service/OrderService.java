package ua.ithillel.car.service.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.car.service.entity.Order;
import ua.ithillel.car.service.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;

    }

    public List<Order> getAllOrders() {
        List<Order> allOrders = new ArrayList<>();
        orderRepository.findAll().forEach(allOrders::add);
        return allOrders;
    }

    public Order findById(int id) {
        return orderRepository.findById(id);
    }

    public void createOrder(Order order) {
        order.setId(generateMaxId() + 1);
        orderRepository.save(order);
    }

    @Transactional
    public void deleteOrder(int id) {
        orderRepository.deleteById(id);
    }

    private int generateMaxId() {
        return getAllOrders().stream()
                .map(Order::getId)
                .mapToInt(Integer::intValue)
                .max()
                .orElse(0);
    }

}
