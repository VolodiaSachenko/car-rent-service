package ua.ithillel.car.service.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.car.service.entity.Car;
import ua.ithillel.car.service.repository.CarRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getAllCars() {
        return new ArrayList<>(carRepository.findByOrderByAvailableDesc());
    }

    public void createCar(Car car) {
        car.setId(generateMaxId() + 1);
        carRepository.save(car);
    }

    public Car selectCarById(long id) {
        return carRepository.findCarById(id);
    }

    @Transactional
    public void deleteCar(long id) {
        carRepository.deleteCarById(id);
    }

    public void updateCarFalse(long id) {
        carRepository.updateFalse(id);
    }

    public void updateCarTrue(long id) {
        carRepository.updateTrue(id);
    }

    private long generateMaxId() {
        return getAllCars().stream()
                .map(Car::getId)
                .mapToInt(Long::intValue)
                .max()
                .orElse(0);
    }

}
